package it.tiwiz.ShareToVoid;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import com.actionbarsherlock.app.SherlockActivity;
import org.holoeverywhere.widget.TextView;

public class VoidActivity extends SherlockActivity {

    private final static String TYPE = "text/plain";

    TextView txtSubject, txtText;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        txtSubject = (TextView)findViewById(R.id.txtSubject);
        txtText = (TextView)findViewById(R.id.txtText);

        //Gets the intent
        Intent inputIntent = getIntent();

        processIntent(inputIntent);
    }

    public void processIntent(Intent inputIntent){

        String inputAction = inputIntent.getAction();
        String inputType = inputIntent.getType();

        if(Intent.ACTION_SEND.equals(inputAction) && (inputType != null))
            shared(inputIntent);
        else
            launcher();
    }

    public void shared(Intent intent){

        String sharedSubject = intent.getExtras().getString(Intent.EXTRA_SUBJECT);

        if(sharedSubject != null)
            txtSubject.setText(Html.fromHtml(sharedSubject));

        String sharedText = intent.getExtras().getString(Intent.EXTRA_TEXT);

        if(sharedText != null)
            txtText.setText(Html.fromHtml(sharedText));

    }

    public void launcher(){

        txtSubject.setText(Html.fromHtml(getString(R.string.txt_subject_about)));
        txtText.setText(Html.fromHtml(getString(R.string.txt_text_about)));
    }
}
